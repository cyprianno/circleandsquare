package cns.main.api;

import cns.main.api.model.GoogleSignInRequest;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.junit.jupiter.api.Assertions.*;

//@MicronautTest
class PlayerControllerTest {

    @Inject
    @Client("/")
    RxHttpClient client;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

//    @Test
    void shouldAuth() {
        GoogleSignInRequest requestBody = new GoogleSignInRequest();
        requestBody.setAccessToken("TokenAccess");
        HttpRequest<GoogleSignInRequest> request = HttpRequest.POST("/player/auth", requestBody);
        HttpResponse<Object> response = client.toBlocking().exchange(request);

        assertNotNull(response);
        assertEquals(HTTP_OK, response.code());
    }

//    @Test
    void shouldGetAuth() {
        HttpRequest<Integer> request = HttpRequest.GET("/player/auth");
        String body = client.toBlocking().retrieve(request);

        assertNotNull(body);
        assertEquals("42", body);
    }
}