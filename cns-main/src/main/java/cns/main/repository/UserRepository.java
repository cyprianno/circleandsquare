package cns.main.repository;

import cns.main.domain.User;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.spring.tx.annotation.Transactional;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class UserRepository {

    @PersistenceContext
    private EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public UserRepository(@CurrentSession EntityManager entityManager,
                               ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Transactional(readOnly = true)
    public Optional<User> findById(@NotNull Long id) {
        return Optional.ofNullable(entityManager.find(User.class, id));
    }

    @Transactional(readOnly = true)
    public Optional<User> findByGoogleId(@NotNull String id) {
        TypedQuery<User> query = entityManager
                .createQuery("SELECT u FROM User u WHERE u.googleId=:googleId", User.class)
                .setParameter("googleId", id);
        return query.getResultList().stream().findFirst();
    }


    @Transactional
    public User save(@NotBlank User user) {
        entityManager.persist(user);
        return user;
    }

    @Transactional
    public void deleteById(@NotNull Long id) {
        findById(id).ifPresent(user -> entityManager.remove(user));
    }

    private final static List<String> VALID_PROPERTY_NAMES = Arrays.asList("id", "name");

//    @Transactional(readOnly = true)
//    public List<User> findAll(@NotNull SortingAndOrderArguments args) {
//        String qlString = "SELECT g FROM Genre as g";
//        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
//            qlString += " ORDER BY g." + args.getSort().get() + " " + args.getOrder().get().toLowerCase();
//        }
//        TypedQuery<Genre> query = entityManager.createQuery(qlString, Genre.class);
//        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
//        args.getOffset().ifPresent(query::setFirstResult);
//
//        return query.getResultList();
//    }

    @Transactional
    public int update(@NotNull Long id, @NotBlank String name) {
        return entityManager.createQuery("UPDATE User u SET name = :name where id = :id")
                .setParameter("name", name)
                .setParameter("id", id)
                .executeUpdate();
    }
}
