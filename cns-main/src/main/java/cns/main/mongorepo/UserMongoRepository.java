package cns.main.mongorepo;

import cns.main.domain.User;
import cns.main.domain.UserMongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import io.micronaut.runtime.ApplicationConfiguration;
import io.micronaut.spring.tx.annotation.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;

import javax.inject.Singleton;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;


@Singleton
@Slf4j
@RequiredArgsConstructor
public class UserMongoRepository {
    private final MongoClient mongoClient;

    private final ApplicationConfiguration applicationConfiguration;

    public Optional<User> findById(@NotNull UUID uuid) {
        MongoDatabase database = mongoClient.getDatabase("circleandsquare");
        MongoCollection<UserMongo> collection = database.getCollection("user", UserMongo.class);
        UserMongo result = collection.find(eq("uuid", uuid)).first();
        return Optional.ofNullable(result);
    }

    @Transactional(readOnly = true)
    public Optional<User> findByGoogleId(@NotNull String id) {
        MongoDatabase database = mongoClient.getDatabase("circleandsquare");
        MongoCollection<UserMongo> collection = database.getCollection("user", UserMongo.class);
        UserMongo result = collection.find(eq("googleId", id)).first();
        return Optional.ofNullable(result);
    }


    public User save(@NotBlank UserMongo user) {
        MongoDatabase database = mongoClient.getDatabase("circleandsquare");
        MongoCollection<UserMongo> collection = database.getCollection("user", UserMongo.class);
        if (user.getUuid() != null) {
            collection.updateOne(eq("uuid", user.getUuid()), new Document("$set", user));
        } else {
            collection.insertOne(user);
        }
        return user;
    }

    public void deleteById(@NotNull Long id) {
//        findById(id).ifPresent(user -> entityManager.remove(user));
    }

    private final static List<String> VALID_PROPERTY_NAMES = Arrays.asList("id", "name");

//    @Transactional(readOnly = true)
//    public List<User> findAll(@NotNull SortingAndOrderArguments args) {
//        String qlString = "SELECT g FROM Genre as g";
//        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
//            qlString += " ORDER BY g." + args.getSort().get() + " " + args.getOrder().get().toLowerCase();
//        }
//        TypedQuery<Genre> query = entityManager.createQuery(qlString, Genre.class);
//        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
//        args.getOffset().ifPresent(query::setFirstResult);
//
//        return query.getResultList();
//    }

    public long update(@NotNull UUID id, @NotBlank String name) {
        MongoDatabase database = mongoClient.getDatabase("circleandsquare");
        MongoCollection<UserMongo> collection = database.getCollection("user", UserMongo.class);
        UpdateResult updateResult = collection.updateOne(eq("id", id), new Document("$set", new Document("name", name)));
        return updateResult.getModifiedCount();
    }
}
