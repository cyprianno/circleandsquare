package cns.main.factories;

import cns.main.domain.Game;

public class GameFactory {
    public Game create() {
        return new Game();
    }
}
