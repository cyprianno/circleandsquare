package cns.main.auth;

import cns.main.domain.User;
import cns.main.domain.UserMongo;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;

import javax.inject.Singleton;
import java.io.IOException;

@Singleton
public class GoogleAuthApi {

    public UserMongo getUserDetails(String accessToken) throws IOException {
//        com.google.api.services.oauth2.Oauth2Scopes.USERINFO_EMAIL
        GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
        Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName(
                "Oauth2").build();
        Userinfo userinfo = oauth2.userinfo().get().execute();
        return toUserMongo(userinfo);
    }

    private User toUser(Userinfo userinfo) {
        User user = new User();
        user.setUsername(userinfo.getName());
        user.setEmail(userinfo.getEmail());
        user.setGoogleId(userinfo.getId());
        return user;
    }

    private UserMongo toUserMongo(Userinfo userinfo) {
        UserMongo user = new UserMongo();
        user.setUsername(userinfo.getName());
        user.setEmail(userinfo.getEmail());
        user.setGoogleId(userinfo.getId());
        return user;
    }

}
