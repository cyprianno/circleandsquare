package cns.main.auth;

import cns.main.domain.User;
import cns.main.domain.UserMongo;
import cns.main.mongorepo.UserMongoRepository;
import cns.main.repository.UserRepository;
import io.micronaut.security.authentication.*;
import io.reactivex.Flowable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.Optional;

import static java.util.List.of;

@Singleton
@Slf4j
@RequiredArgsConstructor
public class GoogleAuthProvider implements AuthenticationProvider {

    private final GoogleAuthApi googleAuthApi;

    private final UserMongoRepository userRepository;

    @Override
    public Publisher<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {
        log.info("Auth: " + authenticationRequest);
        try {
            UserMongo user = googleAuthApi.getUserDetails(String.valueOf(authenticationRequest.getSecret()));
            log.info("User: {}", user);
            Optional<User> byGoogleId = userRepository.findByGoogleId(user.getGoogleId());
            byGoogleId.ifPresentOrElse(user1 -> {
            }, () -> userRepository.save(user));
            return Flowable.just(new UserDetails(user.getUsername(), of("ROLE_USER", "ROLE_PLAYER")));
        } catch (IOException e) {
            return Flowable.just(new AuthenticationFailed());
        }
/*
        if (authenticationRequest.getIdentity().equals("sherlock") &&
                authenticationRequest.getSecret().equals("password")) {
            return Flowable.just(new UserDetails((String) authenticationRequest.getIdentity(), new ArrayList<>()));
        }
        return Flowable.just(new AuthenticationFailed());
*/
    }
}
