package cns.main.domain;

import lombok.Data;

import java.util.UUID;

@Data
public class UserMongo extends User {

    private UUID uuid;

}
