package cns.main.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name="game")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int sizeX;
    private int sizeY;
//    Map<UUID, Board> boards;

    private Long playerId;


    public static class Board {
        Integer[][] fields;
        List<Move> moves;
    }

    public static class Move {
        Move prevMove;
        int toX;
        int toY;
    }

    public static Game createGame() {
        return new Game();
    }

    void makeMove(int fromX, int fromY, int toX, int toY) {

    }
}
