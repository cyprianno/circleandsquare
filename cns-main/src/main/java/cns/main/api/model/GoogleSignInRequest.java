package cns.main.api.model;

import lombok.Data;

@Data
public class GoogleSignInRequest {
    String accessToken;
    String ca;
    String googleId;
    GoogleProfile profileObj;
    String tokenId;
    GoogleToken tokenObj;


    @Data
    private static class GoogleProfile {
        String email;
        String familyName;
        String givenName;
        String name;
        String googleId;
    }

    @Data
    private static class GoogleToken {
        String accessToken;
        String idToken;
    }
}
