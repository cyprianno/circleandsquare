package cns.main.api.model;

import lombok.Data;

import java.util.List;

@Data
public class GameOnListResponse {

    private Long id;
    private int sizeX;
    private int sizeY;
    private Long currentMovePlayer;
    private MyGame myGame;
    private List<PlayerResponse> players;


    @Data
    private static class PlayerResponse {
        Long id;
        String username;
        String shape;
    }

    @Data
    private static class MyGame {
        private String shape;
        private List<WarriorResponse> warriors;
    }

    @Data
    private static class WarriorResponse {
        Long id;
        int posX;
        int posY;
        boolean isAlive;
    }

}
