package cns.main.api;

import cns.main.api.model.GoogleSignInRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lombok.extern.slf4j.Slf4j;

import java.security.Principal;

@Controller("/player")
@Slf4j
public class PlayerController {

    @Post(uri = "/auth")
    @Produces(MediaType.APPLICATION_JSON)
    @Secured(SecurityRule.IS_ANONYMOUS)
    public void auth(GoogleSignInRequest requestData) {
        log.info("grequest: {}",requestData);
    }

    @Get(uri = "/auth")
    @Produces(MediaType.APPLICATION_JSON)
    @Secured(SecurityRule.IS_AUTHENTICATED)
    public Principal getAuth(Principal principal) {
        log.info("principal: {}",principal);
        return principal;
    }

}
