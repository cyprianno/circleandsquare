package cns.main.api;

import cns.main.domain.Game;
import io.micronaut.http.annotation.Controller;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.List;

@Controller("/game")
@Slf4j
public class GameController {

    public Game createGame() {
        return null;
    }

    // PUT /game/XXX/move
    public Game.Move makeMove(int item, int x, int y) {
        return null;
    }

    // /game
    public List<Game> findAll() {
        return Collections.emptyList();
    }

}
