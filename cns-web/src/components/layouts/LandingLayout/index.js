import React, { useState, memo } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Layout, Menu, Icon, Typography } from 'antd'
import { ReactComponent as LogoIcon } from '@static/logo.svg'
import { navigate } from 'gatsby'
import PlayerService from '../../../services/player.service'

const navigateToUrl = (url, item) => {
  navigate(url, {
    state: { item },
  })
}

function handleClick(info) {
  console.log(`clicked ${info.key}`)
  console.log(info)
  console.log(`current: ${window.location.pathname}`)
  navigateToUrl(info.key, info.key)
}

let subscription = null

const LandingLayout = ({ children, playerService }) => {
  const userFromStorage = localStorage.getItem('user')
  let user
  if (userFromStorage) {
    user = JSON.parse(userFromStorage)
  } else {
    user = { username: 'unknown' }
  }

  const [collapsed, toggle] = useState(true)
  const [currentUsername, changeCurrentUsername] = useState(user.username)
  // changeCurrentUsername('unknown user');
  if (!subscription) {
    subscription = playerService.currentPlayer$.subscribe(change => {
      // console.log('user changed', change);
      // console.log(changeCurrentUsername);
      // currentUsername = change.username;
      changeCurrentUsername(change.username)
      // console.log(currentUsername);
      console.log('changed to ', change)
    })
  }
  return (
    <div className='landing-layout'>
      <Layout>
        <Layout.Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
          theme='light'
        >
          <div className='h-20 m-2 flex items-center justify-around'>
            <LogoIcon className='h-10 w-10 mr-2 fill-primary-color' />
            <Typography.Title
              level={4}
              className={classNames('text-primary-color', {
                hidden: collapsed,
              })}
            >
              {process.env.APP_NAME}
            </Typography.Title>
          </div>
          <Menu
            theme='light'
            mode='inline'
            defaultSelectedKeys={['1']}
            className='border-0'
            onClick={handleClick}
            selectedKeys={[window.location.pathname]}
          >
            <Menu.Item key='/'>
              <Icon type='user' />
              <span>main page</span>
            </Menu.Item>
            <Menu.Item key='/GamesPage'>
              <Icon type='video-camera' />
              <span>Games</span>
            </Menu.Item>
          </Menu>
        </Layout.Sider>
        <Layout>
          <Layout.Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className='leading-normal ml-4 text-base'
              type={collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={() => toggle(!collapsed)}
            />
            {currentUsername}
          </Layout.Header>
          <Layout.Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            <div style={{ minHeight: '100vh' }}>{children}</div>
          </Layout.Content>
        </Layout>
      </Layout>
    </div>
  )
}

LandingLayout.propTypes = {
  children: PropTypes.node.isRequired,
  playerService: PropTypes.oneOf([PlayerService]).isRequired,
}

export default memo(LandingLayout)
