import * as React from 'react'

class GameBoard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: null,
      selectedItemId: null,
      game: props.game,
    }
  }

  unselect() {
    this.setState({
      selected: null,
      selectedItemId: null,
    })
  }

  selectCell(target) {
    this.setState({
      selected: target.id,
      selectedItemId: target.getAttribute('val'),
    })
  }

  makeMove(game, selectedId, target) {
    console.log('make move')
    const updatedGame = this.props.gameService.makeMove(
      game,
      selectedId,
      target,
    )
    console.log(updatedGame)
    this.setState({
      game: updatedGame,
      selected: null,
      selectedItemId: null,
    })
  }

  clickCell(el) {
    if (this.state.selected && this.state.selected === el.target.id) {
      this.unselect()
    } else if (el.target.getAttribute('val') !== '0') {
      this.selectCell(el.target)
    } else if (this.state.selected) {
      this.makeMove(this.state.game, this.state.selectedItemId, {
        posX: el.target.getAttribute('posX'),
        posY: el.target.getAttribute('posY'),
      })
    }
  }

  render() {
    const { game } = this.state
    const board = this.props.gameService.prepareBoard(game)
    const outputBoard = []
    const cellStyle = {
      border: '1px solid blue',
    }
    const selectedCellStyle = {
      border: '1px solid red',
    }
    for (let i = 0; i < game.sizeX; i++) {
      const lineBoard = []
      for (let j = 0; j < game.sizeY; j++) {
        const id = `cell-${i}-${j}`
        const cstyle =
          id === this.state.selected ? selectedCellStyle : cellStyle
        console.log(this.state.selected)
        lineBoard.push(
          <div
            onClick={this.clickCell.bind(this)}
            className='ant-col-6'
            id={id}
            style={cstyle}
            posy={i}
            posx={j}
            val={board[i][j]}
          >
            {board[i][j]}
          </div>,
        )
      }
      outputBoard.push(<div className='ant-row'>{lineBoard}</div>)
    }

    const style = { width: '500px' }
    return <div style={style}>{outputBoard}</div>
  }
}

export default GameBoard
