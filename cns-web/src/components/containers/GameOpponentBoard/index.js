import * as React from 'react'

class GameOpponentBoard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: null,
      selectedItemId: null,
      game: props.game,
    }
  }

  shootCell(game, where) {
    this.props.gameService.shootCell(game, where)
  }

  clickCell(el) {
    const { game } = this.state
    const where = {
      posX: el.target.getAttribute('posX'),
      posY: el.target.getAttribute('posY'),
    }
    this.shootCell(game, where)
  }

  render() {
    const { game } = this.state
    const board = this.props.gameService.prepareBoard(game)
    const outputBoard = []
    const cellStyle = {
      border: '1px solid blue',
    }
    const selectedCellStyle = {
      border: '1px solid red',
    }
    for (let i = 0; i < game.sizeX; i++) {
      const lineBoard = []
      for (let j = 0; j < game.sizeY; j++) {
        const id = `cell-${i}-${j}`
        const cstyle =
          id === this.state.selected ? selectedCellStyle : cellStyle
        console.log(this.state.selected)
        lineBoard.push(
          <div
            onClick={this.clickCell.bind(this)}
            className='ant-col-6'
            id={id}
            style={cstyle}
            posy={i}
            posx={j}
          >
            ?
          </div>,
        )
      }
      outputBoard.push(<div className='ant-row'>{lineBoard}</div>)
    }

    const style = { width: '500px' }
    return <div style={style}>{outputBoard}</div>
  }
}

export default GameOpponentBoard
