import * as React from 'react'
import { Button } from 'antd'
import PropTypes from "prop-types";
import GameRowData from "../../../model/GameRowData";

class GameRow extends React.Component {
  clickAction() {
    this.props.clickAction(this.props.game.id)
  }

  render() {
    const { game } = this.props
    const player1Name = this.props.game.players[0]
      ? this.props.game.players[0].username
      : 'empty'
    const player2Name = this.props.game.players[1]
      ? this.props.game.players[1].username
      : 'empty'
    return (
      <div className='ant-row'>
        <div className='ant-col-4'>{game.id}</div>
        <div className='ant-col-4'>{player1Name}</div>
        <div className='ant-col-4'>{player2Name}</div>
        <div className='ant-col-4'>
          <Button onClick={this.clickAction.bind(this)}>Join</Button>
        </div>
      </div>
    )
  }
}

GameRow.defaultProps = {
  games: [],
}

GameRow.propTypes = {
  games: PropTypes.arrayOf(
    PropTypes.oneOf([PropTypes.instanceOf(GameRowData)]),
  ),
  clickAction: PropTypes.func.isRequired,
}
export default GameRow
