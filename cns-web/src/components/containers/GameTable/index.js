import * as React from 'react'
import { navigate } from 'gatsby'
import PropTypes from 'prop-types'
import GameRow from '../GameRow'
import GameRowData from '../../../model/GameRowData'
// import PlayerService from "../../../services/player.service";

class GameTable extends React.Component {
  static selectRow(el) {
    console.log(el)
    navigate(`/GameBoard?gameId=${el}`, {
      state: { el },
    })
  }

  render() {
    const { games } = this.props
    const rows = []
    games.forEach((game, i) => {
      rows.push(
        <GameRow rowkey={i} game={game} clickAction={GameTable.selectRow} />,
      )
    })
    return <div style={{ width: '500px' }}>{rows}</div>
  }
}

GameTable.defaultProps = {
  games: [],
}

GameTable.propTypes = {
  games: PropTypes.arrayOf(
    PropTypes.oneOf([PropTypes.instanceOf(GameRowData)]),
  ),
}

export default GameTable
