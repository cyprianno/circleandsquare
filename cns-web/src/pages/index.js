import React from 'react'
import { Typography } from 'antd'
import LandingLayout from '@layouts/LandingLayout'
import { GoogleLogin } from 'react-google-login'
import PlayerService from '../services/player.service'

let authName = 'unknown'

const ThisPage = () => {
  // deps
  const playerService = new PlayerService()

  const responseGoogle = response => {
    playerService.googleLogin(response).then(() => {
      playerService.getAuth()
      const user = JSON.parse(localStorage.getItem('user'))
      console.log(user)
      authName = user.username
    })
  }

  console.log('starting')

  return (
    <LandingLayout playerService={playerService}>
      <div className='flex flex-col items-center'>
        {authName}
        <GoogleLogin
          clientId='404757425112-u4ukl0c3grkteqmd414c0b2hqeldcakg.apps.googleusercontent.com'
          buttonText='Login'
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy='single_host_origin'
        />
        <Typography.Title className='text-primary-color'>
          Fast in every way that matters
        </Typography.Title>
        <Typography.Title level={4} className='text-gray-600'>
          Gatsby is a free and open source framework based on React that helps
          developers build blazing fast websites and apps
        </Typography.Title>
        <img src='/cover.png' alt='logo' className='mt-10' />
      </div>
    </LandingLayout>
  )
}

export default ThisPage
