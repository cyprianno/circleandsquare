// import { GoogleLogin } from 'react-google-login'
// import { Typography } from 'antd'
import React from 'react'
import LandingLayout from '../../components/layouts/LandingLayout'
// import GameTable from '../../components/containers/GameTable'
import GameService from '../../services/game.service'
import PlayerService from '../../services/player.service'
import GameBoard from '../../components/containers/GameBoard'
import GameOpponentBoard from '../../components/containers/GameOpponentBoard'

const ThisPage = () => {
  console.log('starting games')
  const gameId = 123
  const playerService = new PlayerService()
  const gameService = new GameService()
  const game = gameService.getGame(gameId)

  return (
    <LandingLayout playerService={playerService}>
      <div className='flex flex-col items-center'>
        <GameBoard game={game} gameService={gameService} />
      </div>
      <div style={{ margin: '50px' }} />
      <div className='flex flex-col items-center'>
        <GameOpponentBoard game={game} gameService={gameService} />
      </div>
    </LandingLayout>
  )
}

export default ThisPage
