// import { GoogleLogin } from 'react-google-login'
// import { Typography } from 'antd'
import React from 'react'
import LandingLayout from '../../components/layouts/LandingLayout'
import GameTable from '../../components/containers/GameTable'
import GameService from '../../services/game.service'
import PlayerService from '../../services/player.service'

const ThisPage = () => {
  console.log('starting games')
  const playerService = new PlayerService()
  const games = new GameService().getGames()

  return (
    <LandingLayout playerService={playerService}>
      <div className='flex flex-col items-center'>
        <GameTable games={games} />
      </div>
    </LandingLayout>
  )
}

export default ThisPage
