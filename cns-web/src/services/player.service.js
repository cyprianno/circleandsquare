import axios from 'axios'
import { Subject } from 'rxjs'
import Configuration from './configuration'

class PlayerService {
  currentPlayer$ = new Subject()

  googleLogin(googleLoginResponse) {
    console.log(googleLoginResponse)

    axios.post(`${Configuration.API_URL}/player/auth`, googleLoginResponse)

    return axios
      .post(`${Configuration.API_URL}/login`, {
        username: 'google',
        password: googleLoginResponse.accessToken,
      })
      .then(response => {
        localStorage.setItem('user', JSON.stringify(response.data))
        this.currentPlayer$.next(response.data)
        console.log(response)
      })
  }

  static getAuth() {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    axios.get(`${Configuration.API_URL}/player/auth`, {
      headers: {
        Authorization: `Bearer ${user.access_token}`,
      },
    })
    // this._currentPlayer$.next(user);
    return user
  }

  static getAuthPromise() {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log(user)
    return Promise.resolve(user)
  }
}

export default PlayerService
