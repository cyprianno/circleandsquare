// import axios from 'axios'
// import { useReducer } from 'react'
// import Configuration from './configuration'
// import {BehaviorSubject} from "rxjs";

class GameService {
  mockPlayers = [
    {
      id: 100,
      username: 'BOT',
      shape: 'CIRCLE',
    },
    {
      id: 1,
      username: 'Cyprian',
      shape: 'SQUARE',
    },
  ]

  gamesMock = [
    { id: '1231213', availableToPLay: true, players: this.mockPlayers },
    { id: '1231214', availableToPLay: true, players: this.mockPlayers },
    { id: '1231215', availableToPLay: true, players: this.mockPlayers },
  ]

  oneGameMock = {
    sizeX: 3,
    sizeY: 3,
    currentMovePlayer: 1,
    players: this.mockPlayers,
    myWarriors: [
      {
        id: 1,
        posX: '0',
        posY: '0',
        isAlive: true,
      },
      {
        id: 2,
        posX: '1',
        posY: '0',
        isAlive: true,
      },
      {
        id: 3,
        posX: '2',
        posY: '0',
        isAlive: true,
      },
    ],
  }

  // const currentPlayer = new BehaviorSubject();

  getGames() {
    return this.gamesMock
  }

  getGame() {
    return this.oneGameMock
  }

  static prepareBoard(game) {
    const board = []
    for (let i = 0; i < game.sizeX; i += 1) {
      board[i] = []
      for (let j = 0; j < game.sizeY; j += 1) {
        board[i][j] = 0
      }
    }
    game.myWarriors.forEach(warrior => {
      board[warrior.posY][warrior.posX] = warrior.id
    })
    return board
  }

  static makeMove(game, el, destination) {
    game.myWarriors.map(warrior => {
      if (warrior.id === Number(el)) {
        return {
          ...warrior,
          posX: destination.posX,
          posY: destination.posY,
        }
      }
      return warrior
    })
    return game
  }

  static shootCell(game, where) {
    console.log('shooting: ', where)
  }

  static addBot(game) {
    return game
  }
}

export default { GameService }
