db = db.getSiblingDB('devDb');
db.createUser(
{
    user: "devUser",
    pwd: "PASS",
    roles: [
      { role: "readWrite", db: "devDb" }
    ]
});

db.createRole({
    role: "readWriteSystem",
    privileges: [{
        resource: {db: "devDb", collection: "system.indexes"},
        actions: ["changeStream", "collStats", "convertToCapped", "createCollection", "createIndex", "dbHash", "dbStats", "dropCollection", "dropIndex", "emptycapped", "find", "insert", "killCursors", "listCollections", "listIndexes", "planCacheRead", "remove", "renameCollectionSameDB", "update"]
    }],
    roles: []
});
db.grantRolesToUser('devUser', ['readWriteSystem']);
